package com.example.proj;

import java.io.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

// Сервлет для авторизации пользователя
// Логин для администратора: admin/pass
// Логин для пользователя: user/user_pass
@WebServlet(name = "auth-servlet", value = "/auth")
public class AuthServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        // Получаем логин и пароль пользователя из запроса
        String n = request.getParameter("username");
        String p = request.getParameter("userpass");

        // Проверяем логин и пароль на соответствие с логином и паролем администратора
        ConnectionStatus cs1 = AuthDAO.validateAdmin(n, p);
        // Проверяем логин и пароль на соответствие с логином и паролем пользователя
        ConnectionStatus cs2 = AuthDAO.validateUser(n, p);

        // Если введен неверный пароль для любой из учетных записей, перенаправляем на страницу с ошибкой
        if (cs1 == ConnectionStatus.WRONG_PASSWORD || cs2 == ConnectionStatus.WRONG_PASSWORD){
            RequestDispatcher rd = request.getRequestDispatcher("wrong-login-servlet");
            rd.forward(request, response);
        }
        // Если введен правильный логин и пароль для администратора, перенаправляем на страницу администратора
        else if (cs1 == ConnectionStatus.OK) {
            RequestDispatcher rd = request.getRequestDispatcher("admin-servlet");
            rd.forward(request, response);
        }
        // Если введен правильный логин и пароль для пользователя, перенаправляем на страницу приветствия
        else if (cs2 == ConnectionStatus.OK) {
            RequestDispatcher rd = request.getRequestDispatcher("welcome-servlet");
            rd.forward(request, response);
        }
        // Если введен неверный логин или пароль, перенаправляем на страницу с ошибкой
        else {
            RequestDispatcher rd = request.getRequestDispatcher("wrong_login.jsp");
            rd.include(request, response);
        }
    }

    public void destroy() {
    }
}

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Приемная комиссия вуза</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>

<div id="header">
    <h1>Добро пожаловать в приемную комиссию вуза</h1>
</div>

<!-- Панель навигации или меню -->
<div id="nav">
    <ul>
        <li><a href="index.jsp">Главная</a></li>
        <li><a href="about.jsp">О нас</a></li>
        <li><a href="contact.jsp">Контакты</a></li>
    </ul>
</div>

<!-- Секция контента -->
<div id="section">
    <!-- Форма входа для пользователя -->
    <div id="user-login">
        <h2>Вход для абитуриентов</h2>
        <form action="auth" method="post">
            <div class="form-group">
                <label for="username">Логин:</label>
                <input type="text" id="username" name="username" required>
            </div>
            <div class="form-group">
                <label for="password">Пароль:</label>
                <input type="password" id="password" name="password" required>
            </div>
            <div class="form-group">
                <input type="submit" value="Вход">
            </div>
        </form>
    </div>

    <!-- Форма входа для администратора -->
    <div id="admin-login">
        <h2>Вход для администратора</h2>
        <form action="auth" method="post">
            <div class="form-group">
                <label for="admin-username">Логин:</label>
                <input type="text" id="admin-username" name="username" required>
            </div>
            <div class="form-group">
                <label for="admin-password">Пароль:</label>
                <input type="password" id="admin-password" name="password" required>
            </div>
            <div class="form-group">
                <input type="submit" value="Вход">
            </div>
        </form>
    </div>
</div>

<!-- Подвал сайта -->
<div id="footer">
    <p>&copy; 2023 Приемная комиссия вуза</p>
</div>

</body>
</html>
